package com.ghosthio.countdowntimer

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
     // var count: CountDownTimer? = null
    var clickCount:Int =0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
   /* fun timerButton(button: View) {
        if (count != null) {
            count?.cancel()
            count = null
            launchView.text = " décollage annulée"
        } else {

            count = object : CountDownTimer(5000, 100) {
                override fun onTick(p0: Long) {
                    launchView.text = "${p0 / 1000}"
                }

                override fun onFinish() {
                    launchView.text = " décollage "
                }
            }
            count?.start()
        }
    }*/
    fun buttonLaunchTouched(button:View) {
        clickCount++
        launchView.text = "$clickCount clic(s)"

    }

    fun settingsButtonTouched(button: View) {
        val intent = Intent(this, MainActivity2::class.java)
        intent.putExtra("clickCount",clickCount)
        startActivity(intent)
    }
}